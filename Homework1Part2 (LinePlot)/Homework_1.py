import matplotlib.pyplot as plt
import sys
import csv
data = {"year": [], "Europe":[], "North America" :[], "South America" : [], "Asia" :[], "Africa" : [], "Australia" : [],"Total Value" : []}
legend = {""}
years = []
Africa = [] 
Europe = []
Asia = []
North_America = []
South_America = [] 
Australia = []
url = 'goats2.csv'
onemillion = 10**6
goats = csv.DictReader(open(url))
for item in goats:  
    if item['Country or Area'] == 'Africa +':
        x = int(item['Value'])/onemillion
        Africa.append(x)
    if item['Country or Area'] == 'Europe +':
         x = int(item['Value'])/onemillion
         Europe.append(x)
    if item['Country or Area'] == 'Northern America +':
         x = int(item['Value'])/onemillion
         North_America.append(x)
    if item['Country or Area'] == 'South America +':
         x = int(item['Value'])/onemillion
         South_America.append(x)
    if item['Country or Area'] == 'Asia +':
         x = int(item['Value'])/onemillion
         Asia.append(x)
    if item['Country or Area'] == 'Australia and New Zealand +':
         x = int(item['Value'])/onemillion
         Australia.append(x)
    data["year"].append(int(item["Year"]))
import numpy as np
years = list(set(data["year"]))
#Asia = np.array(Asia)

#years = np.arange(min(years), max(years), 1.0)
plt.plot(years, Asia)
plt.plot(years, Africa)
plt.plot(years, Europe)
plt.plot(years, Australia)
plt.plot(years, North_America)
plt.plot(years, South_America)
plt.yticks(np.arange(0, 525, 25.0))
plt.xticks(np.arange(1961, 2008, 5.0))
plt.ylim([0, 500])
plt.ylabel('Goat Imports by Millions')
plt.xlabel('Years')
#plt.xticks(years)
plt.title('Imports of Goats Across Main Continents, 1961- 2007')
plt.legend(["Asia", "Africa", "Europe",  "Australia", "North America", "South America"], loc=2, bbox_to_anchor=(1, 1))
plt.show()

    
