import csv
from matplotlib import pyplot as plt
data = csv.DictReader(open('crabs.csv'))
ax = plt.subplot(111)
bluemalex =[]
bluemaley = []
bluefemalex = []
bluefemaley = []
orangemalex = []
orangemaley = []
orangefemalex = []
orangefemaley = []
for i in data:
    if i['sp'] == 'O':
        if i['sex'] == 'M':
            orangemalex.append(float(i['CL']))
            orangemaley.append(float(i['CW']))
            #ax.scatter(i['CL'], i['CW'], s = 100, c='Chocolate')
        if i['sex'] == 'F':
            orangefemalex.append(i['CL'])
            orangefemaley.append(float(i['CW']))
            #ax.scatter(i['CL'], i['CW'], s = 100, c='Orange')
    if i['sp'] == 'B':
        if i['sex'] == 'M':
            bluemalex.append(float(i['CL']))
            bluemaley.append(float(i['CW']))
            #ax.scatter(i['CL'], i['CW'], s=100, c='LightBlue')
        if i['sex'] == 'F':
            bluefemalex.append(float(i['CL']))
            bluefemaley.append(float(i['CW']))
           # ax.scatter(i['CL'], i['CW'], s=100, c='DarkBlue')
import numpy as np
plt.title('Analysis of Carcass Length and Carcass Width of Different Crab Species')
plt.xlabel('Carcass Length')
plt.ylabel('Carcass Width')
ax.scatter(orangemalex, orangemaley, s = 200, c='Orange', label='Orange Male')
ax.scatter(orangefemalex, orangefemaley, s = 200, c='Chocolate', label='Orange Female')
ax.scatter(bluemalex, bluemaley, s = 200, c = 'LightBlue', label = 'Blue Male')
ax.scatter(bluefemalex, bluefemaley, s = 200, c = 'Blue', label = 'Blue Female')
plt.xticks(np.arange(min(bluefemalex)-1, max(orangemalex)+1, 3.0))
plt.yticks(np.arange(min(bluefemaley)-1, max(orangemaley)+1, 2.0))
ax.legend(bbox_to_anchor=(1, 1), loc=2, title='Species of Crab')
plt.show()
            #ax.scatter(i['CL'], i['CW'], c='Orange')
       # if i['sex'] == 'F':
           # ax.scatter(i['CL'], i['CW'], c='RedOrange')
#plt.show()
        