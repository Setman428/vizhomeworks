
import numpy as np
import matplotlib
#matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib as mpl

# generate some random data

#first create the figure


#create a grid of 1 row and 1 column for the plot
#[left, bottom, width, height]
gs = mpl.gridspec.GridSpec(1,1)
#put a plot in the first row, first column
ax = plt.subplot(111)

#Have only two spines visible and set properties
ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_linewidth(.5)
ax.xaxis.set_ticks_position('bottom')

ax.spines['right'].set_visible(False)
ax.spines['left'].set_linewidth(0.5)
ax.yaxis.set_ticks_position('left')

#set tick properties
ax.xaxis.set_tick_params('major', length=2, labelsize=20)
ax.yaxis.set_tick_params('major', length=2, labelsize=20)

#plot data and customize look of plot

#set axis limits 


#scale the view to show all datapoints
ax.autoscale_view()

#set labels
ax.set_xlabel(r'voltage (V, $\mu$V)', fontsize=20, family='sans-serif')
ax.set_ylabel('luminescence (L)', fontsize=20, family='sans-serif')


#matplotlib.pyplot.show()
data = np.recfromcsv(open('crabs.csv'))
x = []
y = []
z = []
for i in data:
        x.append(i['cl'])
        y.append(i['cw'])
        z.append(i['fl'])
        
#HW Part 1_1a
ax = plt.subplot(111)
ax.set_xlabel(r'Carcas length (mm)', fontsize=12, family='sans-serif')
ax.set_ylabel('Carcas width (mm)', fontsize=12, family='sans-serif')
plt.scatter(x, y)
plt.title('Carcase Length vs. Carcase Width')
ax.autoscale_view()
plt.show(1)

#HW Part 1_1b
plt.scatter(x, y)
plt.scatter(x, z)
plt.title('Carcase Length vs. Carcase Width vs. Body Length')
ax2 = ax.twinx()
ax.set_xlabel(r'Carcas Length (mm)', fontsize=12, family='sans-serif')
ax.set_ylabel('Carcas width (mm)', fontsize=12, family='sans-serif')
ax2.set_ylabel('Body Depth (mm)', fontsize=12, family='sans-serif')
ax.autoscale_view()
plt.show(2)

#HW_Part1_1c
for i in range(len(x)):
    plt.scatter(x[i], y[i], s=z[i]*20)
plt.title('Carcase Length vs. Carcase Width vs. Body Length')
ax2 = ax.twinx()
ax.set_xlabel(r'Carcas Length (mm)', fontsize=12, family='sans-serif')
ax.set_ylabel('Carcas width (mm)', fontsize=12, family='sans-serif')
ax2.set_ylabel('Body Depth (mm)', fontsize=12, family='sans-serif')
ax.autoscale_view()
ax.axis([min(x)-1, max(x)+1, min(y)-1, max(y)+1])
ax2.axis([min(x)-1, max(x)+1, min(z)-1, max(z)+1])
plt.show(3)
plt.savefig("plot1.svg")
plt.title('Carcase Length vs. Carcase Width vs. Body Length')
for i in range(len(x)):
    plt.scatter(x[i], y[i], c=z[i], s=40, norm=mpl.colors.SymLogNorm(linthresh=10, vmin=-1e3, vmax=1e3))
zticks = [min(z)-1, max(z)/2, max(z)+1]
plt.colorbar(ticks=zticks)
ax2 = ax.twinx()
ax.set_xlabel(r'Carcas Length (mm)', fontsize=12, family='sans-serif')
ax.set_ylabel('Carcas width (mm)', fontsize=12, family='sans-serif')
#ax2.set_ylabel('Body Depth (mm)', fontsize=12, family='sans-serif')
ax.autoscale_view()
ax.axis([min(x)-1, max(x)+1, min(y)-1, max(y)+1])
plt.show(4)
plt.savefig("plot2.svg")
