import matplotlib.pyplot as plt
import csv
import numpy as np
goats = []
turkeys = []
cattles = []
sheeps = []
pigs = []
ducks = []
chickens = []
continents = []
goatsurl = "goats.csv"
turkeyurl = "turkey.csv"
cattleurl = "cattle.csv"
sheepurl = "sheep.csv"
pigsurl = "pigs.csv"
ducksurl = "ducks.csv"
chickenurl = "chicken.csv"
#N = 61
#x = np.arange(N)
ax = plt.subplot(111)
width = 0.10
goat = csv.DictReader(open(goatsurl))
turkey = csv.DictReader(open(turkeyurl))
cattle = csv.DictReader(open(cattleurl))
sheep = csv.DictReader(open(sheepurl))
pig = csv.DictReader(open(pigsurl))
duck = csv.DictReader(open(ducksurl))
chicken = csv.DictReader(open(chickenurl))
onemillion = 10**(6)
for item in goat:
    x = float(item['Value']) / onemillion
    goats.append(x)
for item in turkey:
    x = float(float(item['Value']) / onemillion)
    turkeys.append(x)
for item in cattle:
    x = float(item['Value']) / onemillion
    cattles.append(x)
for item in sheep:
    x = float(item['Value']) / onemillion
    sheeps.append(x)
for item in pig:
     x = float(item['Value']) / onemillion
     pigs.append(x)
for item in duck:
     x = float(item['Value']) / onemillion
     ducks.append(x)
for item in chicken:
     x = float(item['Value']) / onemillion
     chickens.append(x)
    #continents.append(item['Country or Area'])
continents = ['Africa', 'Asia', 'Australia', 'Europe', 'N. America', 'S. America']
labels = [item.get_text() for item in ax.get_xticklabels()]
axes = plt.gca()
ax.set_xticklabels(labels)
N = 6
figure = plt.gcf()

    
figure.set_size_inches(500, 500)
x = np.arange(N)
ax.bar(x - width*3, turkeys, width, color = 'black')
ax.bar(x - width*2, chickens, width, color = 'orange')
ax.bar(x - width, sheeps, width, color = 'blue')
ax.bar(x + 0.00, goats, width, color = 'red')
ax.bar(x + width, ducks, width, color = 'green')
ax.bar(x + width*2, cattles, width, color = 'yellow')
ax.bar(x + width*3, pigs, width, color = 'grey')

ax.autoscale_view()
plt.title('Animals Imported to Various Continents')
plt.legend(['Turkeys', 'Chickens', 'Sheep', 'Goats', 'Ducks', 'Cattle', 'Pigs'], title = 'Animals Imported')
xtickpoints = [0, 1, 2, 3, 4, 5]
plt.yticks(np.arange(float(min(turkeys)), max(pigs)+60, 20.0))
plt.xlabel('Continents', labelpad = 20)
plt.ylabel('Animals imported in millions', labelpad = 20)
plt.xticks(xtickpoints, continents, ha='left')
plt.show()