from itertools import cycle

from bokeh._glyph_functions import text
try:
    import Tkinter as tk
except:
    import tkinter as tk
    
    
#
class App(tk.Tk):   
    def __init__(self, image_files, desc, x, y, delay):
        # the root will be self
        tk.Tk.__init__(self)
       #set x, y position only
        self.geometry('+{}+{}'.format(x, y))
        self.delay = delay
        # allows repeat cycling through the pictures
    # store as (img_object, img_name) tuple
        self.pictures = cycle((tk.PhotoImage(file=image), image) for image in image_files)
        #titl = tk.Text(description)
        self.picture_display = tk.Label(self)
        w2 = tk.Label(self, justify = 'left', padx = 10, text = description).pack(side = "left")
        
        self.picture_display.pack()
        
        
    def show_slides(self):
        img_object, img_name = next(self.pictures)
        self.picture_display.config(image=img_object)
            # shows the image filename, but could be expanded        
            # to show an associated description of the image
        self.title(img_name)
        #raw_input("Press Enter to Continue")
        self.after(self.delay, self.show_slides)
        
    def run(self):
        self.mainloop()
        
        # set milliseconds time between slides
delay = 3500

# get a series of gif images you have in the working folder
# or use full path, or set directory to where the images are
image_files = [
'timeseries.gif',
'2d_vector_field.gif',
'3D_Shapes.gif',
'Scalar_field_3D.gif',
'mesh_ex1.gif',
'graph.gif'
]
description = [
'''The first is a 1 dimensional time series. It represents the fluctation of data over time in a field \n'''
'''The second is a 2d vector field. \n They have a center point that the arrows emanate from. \n Since mangetism has polarity, this makes this a popular ''' 
        
        
]
# upper left corner coordinates of app window
x = 100
y = 50

app = App(image_files, description, x, y, delay)
app.show_slides()
app.run()

