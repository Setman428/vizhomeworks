import scipy.io 
from matplotlib import pyplot as plt
data = scipy.io.loadmat('AQUA.2013089.2055.mat')

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.imshow(data['B1'])

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
cmap = plt.get_cmap('hot')
ax.imshow(data['B1'], cmap = cmap)

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
cmap = plt.get_cmap('hot')
fig_image = ax.imshow(data['B1'], cmap = cmap)
cax = fig.colorbar(fig_image, ax = ax)
cax.set_label("Reflectance")

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
fig_image = ax.hist(data['B1'])

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
cmap = plt.get_cmap('hot')
fig_image = ax.imshow(data['B6'], cmap = cmap)
cax = fig.colorbar(fig_image)
cax.set_label("Reflectance")

import numpy as np
N = 914*667
spercentage = .45
numsamples = int(spercentage*N)
samplesInds =range(1,N)
np.random.shuffle(samplesInds)
samplesInds = samplesInds[0:numsamples]
b1 = data['B1'].ravel()[samplesInds]
b6 = data['B6'].ravel()[samplesInds]
b7 = data['B7'].ravel()[samplesInds]
plt.scatter(b1, b6, s = 10, c = 'red', marker='o', alpha=0.2)
plt.title('A Comparison of B1 and B6 Reflectance')
plt.xlabel('B1 Reflectance')
plt.ylabel('B6 Reflectance')
plt.show(6)

from scipy import stats
regression=np.polyfit(b1, b6, deg=5)
#regressionbestfit = np.polyval(regression, min(b1)-1)
plt.plot(regression, c = 'red')
plt.xlabel('b1 samples')
plt.ylabel('b6 samples')
plt.legend(['linear regression line'])
plt.title('Linear regression plot')
slope = round(stats.linregress(b1, b6)[0],2)
pearson = round(stats.pearsonr(b1, b6)[0],2)
plt.text(1, 1, 'Pearson = %s \n Slope = %s' %(pearson, slope), va='bottom', ha = 'left')
plt.show(7)

regression=np.polyfit(b7, b6, deg=5)
plt.xlabel('b7 samples')
plt.ylabel('b6 samples')
plt.legend(['linear regression line'])
slope = round(stats.linregress(b7, b6)[0],2)
pearson = round(stats.pearsonr(b7, b6)[0],2)
plt.text(1, 1, 'Pearson = %s \n Slope = %s' %(pearson, slope), va='bottom', ha = 'left')
plt.title('Linear regression plot')
plt.plot(regression, c ='blue')
plt.show(8)