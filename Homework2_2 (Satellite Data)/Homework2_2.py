from scipy.io import loadmat
from matplotlib import pyplot as plt
data = loadmat('AQUA.2013089.2055.mat')

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.imshow(data['B1'])

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
cmap = plt.get_cmap('hot')
ax.imshow(data['B1'], cmap = cmap)

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
cmap = plt.get_cmap('hot')
fig_image = ax.imshow(data['B1'], cmap = cmap)
cax = fig.colorbar(fig_image, ax = ax)
cax.set_label("Reflectance")

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
fig_image = ax.hist(data['B1'])

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
cmap = plt.get_cmap('hot')
fig_image = ax.imshow(data['B6'], cmap = cmap)
cax = fig.colorbar(fig_image)
cax.set_label("Reflectance")