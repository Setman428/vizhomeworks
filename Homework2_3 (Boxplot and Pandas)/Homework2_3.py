from scipy.io import loadmat
from matplotlib import pyplot as plt
def toFahrenheit(temperature):
    return ((temperature * 1.8) - 459.67)
if __name__ == '__main__':
    paris = loadmat('Paris.mat')
    saopaolo = loadmat('SaoPaulo.mat')
    sydney = loadmat('Sydney.mat')
    johannesburg = loadmat('Johannesburg.mat')
    signapore = loadmat('Singapore.mat')
    vancouver = loadmat('Vancouver.mat')
    temperatures = [toFahrenheit(paris['temperature']), toFahrenheit(saopaolo['temperature']), 
                    toFahrenheit(sydney['temperature'])
                    , toFahrenheit(johannesburg['temperature']),
                     toFahrenheit(signapore['temperature']), toFahrenheit(vancouver['temperature'])] #Only want temperatures
    names = ['Paris', 'Sao Paolo', 'Sydney', 'Johannesburg', 'Signapore', 'Vacouver']
    fig = plt.figure(1, figsize=(9,6))
    ax = fig.add_subplot(111)
    #bp = ax.boxplot(temperatures)
    currentticks = ax.get_xticks().tolist()
    for i in range(len(names)):
        currentticks[i] = names[i]
   
    plt.boxplot(temperatures)
    plt.title('Temperatures in cities across the world')
    plt.xlabel('City')
    plt.ylabel('Temperatures in Fahrenheit')
    ax.set_xticklabels(currentticks, rotation = 45)
    
    plt.tight_layout()
    plt.show(1)
    
    temperatureslast = [toFahrenheit(paris['temperature'][-20000:]), toFahrenheit(saopaolo['temperature'][-20000:]), 
                    toFahrenheit(sydney['temperature'][-20000:])
                    , toFahrenheit(johannesburg['temperature'][-20000:]),
                     toFahrenheit(signapore['temperature'][-20000:]), toFahrenheit(vancouver['temperature'][-20000:])] #Only want temperatures
    temperaturesfirst = [toFahrenheit(paris['temperature'][0:20000]), toFahrenheit(saopaolo['temperature'][0:20000]), 
                    toFahrenheit(sydney['temperature'][0:20000])
                    , toFahrenheit(johannesburg['temperature'][0:20000]),
                     toFahrenheit(signapore['temperature'][0:20000]), toFahrenheit(vancouver['temperature'][0:20000])]
    f, axarr = plt.subplots(2)
    axarr[0].boxplot(temperaturesfirst)
    axarr[0].set_title('First 20,000 temperatures')
    axarr[1].boxplot(temperatureslast)
    axarr[1].set_title('Last 20,000 temperatures')
    axarr[0].set_xlabel('City')
    axarr[0].set_xticklabels(currentticks, rotation=45)
    axarr[1].set_xticklabels(currentticks, rotation=45)
    axarr[1].set_xlabel('City')
    axarr[0].set_ylabel('Temperatures in Fahrenheit')
    axarr[1].set_ylabel('Temperatures in Fahrenheit')
    plt.tight_layout()
    plt.show(2)
    
    
    
        
        
