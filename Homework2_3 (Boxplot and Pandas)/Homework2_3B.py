import pandas as pd
from scipy.io import loadmat
from matplotlib import pyplot as plt
import numpy as np
def toFahrenheit(temperature):
    return ((temperature * 1.8) - 459.67)
paris = loadmat('Paris.mat')
#saopaolo = loadmat('SaoPaulo.mat')
#sydney = loadmat('Sydney.mat')
#johannesburg = loadmat('Johannesburg.mat')
#signapore = loadmat('Singapore.mat')
#vancouver = loadmat('Vancouver.mat')
timeyears = []
timemonths = []
temperatures = []
for i in paris["time"]:
    timeyears.append(int(i.split('-')[0]))
    timemonths.append(i.split('-')[1])
parisdf = pd.DataFrame()
parisdf['Years'] = timeyears
parisdf['Months'] = timemonths
parisdf['Temperature'] = toFahrenheit(paris["temperature"])
def sortbyyear(year, df):
    winterdata = df[(df['Years'] == year) | (df['Months'] == '12') & (df['Months'] == '01') | (df['Months'] == '02') ]
    springdata = df[(df['Years'] == year) | (df['Months'] == '03') & (df['Months'] == '04') | (df['Months'] == '05') ]
    summerdata = df[(df['Years'] == year) | (df['Months'] == '06') & (df['Months'] == '07') | (df['Months'] == '08') ]
    falldata = df[(df['Years'] == year) | (df['Months'] == '09') & (df['Months'] == '10') | (df['Months'] == '11') ]
    winteraverage = winterdata["Temperature"].mean()
    springaverage = springdata["Temperature"].mean()
    summeravg = summerdata["Temperature"].mean()
    fallavg = falldata["Temperature"].mean()
    return (winteraverage, springaverage, summeravg, fallavg)
wintertemp =[]
springtemp = []
summertemp = []
falltemp = []
years = []
i = 1948
while i < 2009:
     wintertemp.append(sortbyyear(i, parisdf)[0])
     springtemp.append(sortbyyear(i, parisdf)[1])
     summertemp.append(sortbyyear(i, parisdf)[2])
     falltemp.append(sortbyyear(i, parisdf)[3])
     i += 1
ax = plt.subplot(111)
ax.plot(wintertemp)
ax.plot(springtemp)
ax.plot(summertemp)
ax.plot(falltemp)
ax.legend(['Winter', 'Spring', 'Summer', 'Fall'], loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('Years')
years = ['1948', '1958', '1968', '1978', '1988', '1998', '2008']
currentticks = ax.get_xticks().tolist()
for i in range(len(years)):
    currentticks[i] = years[i]
ax.set_xticklabels(currentticks)
plt.ylabel('Temperatures in Fahrenheit')
plt.title('Seasonal Temperatures for Paris, 1948 - 2008')

plt.show()
