# README #

Welcome to my repository! I'm John Settineri, and this represents all of the work I did for the visualization class on the Homeworks and the Tutorial Assignment. 

### What is this repository for? ###

This repository is for all of the visualization homework assignments done. I wanted to keep it all in one place. 

### What's contained in here? ###
* Homework 1 (Basic MatplotLib)
* Homework 2 (Advanced Matplotlib)
* Homework 3 (D3)
* Tutorial on the vincent library in Python.



### Contribution guidelines ###
Anybody can contribute if they want to. Not my concern at the moment. 

### Who do I talk to? ###

* John Settineri, jsettin000@citymail.cuny.edu--Student
* Michael Grossberg, grossberg@cs.ccny.cuny.edu --Professor